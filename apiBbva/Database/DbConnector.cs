﻿using Microsoft.EntityFrameworkCore;
using apiBbva.Models;

namespace apiBbva.Database
{
    public class DbConnector : DbContext
    {
        public DbConnector(DbContextOptions<DbConnector> options) : base(options)
        {

        }
        public DbSet<Users> Users { get; set; }
    }


}
