﻿using Microsoft.AspNetCore.Mvc;
using apiBbva.Database;
using apiBbva.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiBbva.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DbConnector _context;

        public UserController(DbConnector context)
        {
            _context = context;
        }
        // GET: api/<UserController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost("saveUser")]
        public async Task<IActionResult> SaveUser([FromBody] Users formData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                using (var conn = new NpgsqlConnection(_context.Database.GetConnectionString()))
                {
                    conn.Open();

                    using (var cmd = new NpgsqlCommand("SELECT bbva.save_user(@id, @username);", conn))
                    {
                        cmd.Parameters.AddWithValue("id", formData.Id);
                        cmd.Parameters.AddWithValue("username", formData.username);

                        // Ejecuta la función PostgreSQL y obtiene el resultado
                        var result = cmd.ExecuteScalar();

                        // Si result contiene el resultado deseado, puedes devolverlo en la respuesta
                        return Ok(result);
                    }
                }
            }
            catch(Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        [HttpPost]
        /*
        public async Task<ActionResult<User>> CreateUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }*/

        // POST api/<UserController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
