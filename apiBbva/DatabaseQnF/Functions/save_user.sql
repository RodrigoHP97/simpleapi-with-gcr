--DROP FUNCTION bbva.save_user;
CREATE OR REPLACE FUNCTION bbva.save_user(
    IN p_id INT,
    IN p_user_name VARCHAR(255)
) RETURNS TABLE (
    id INT,
    name VARCHAR(255)
) AS
$$
BEGIN
    -- Realiza la inserción en la tabla users
    INSERT INTO bbva.users (user_id, user_name) VALUES (p_id, p_user_name);

    -- Retorna una consulta de los datos registrados
    RETURN QUERY SELECT user_id as pk, user_name as name FROM bbva.users WHERE user_id = p_id;
END;
$$
LANGUAGE plpgsql;