-- Crear la tabla "usuarios" en el esquema "bbva"
CREATE TABLE IF NOT EXISTS bbva.users (
    user_id serial PRIMARY KEY,
    user_name varchar(255) NOT NULL
);

-- Agregar una restricción UNIQUE a la columna "id"
ALTER TABLE bbva.users
ADD CONSTRAINT unique_users_id UNIQUE (user_id);