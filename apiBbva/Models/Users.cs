﻿using System.ComponentModel.DataAnnotations;

namespace apiBbva.Models
{
    public class Users
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string username { get; set; }

    }
}
